using System.Linq;
using NUnit.Framework;

namespace Anagram
{
    public class AnagramShould
    {
        [TestCase("ab", new [] {"ab", "ba"})]
        [TestCase("ac", new [] {"ac", "ca"})]
        [TestCase("ca", new [] {"ac", "ca"})]
        [TestCase("bc", new [] {"cb", "bc"})]
        [TestCase("zy", new [] {"yz", "zy"})]
        [TestCase("aa", new [] {"aa"})]
        [TestCase("bb", new [] {"bb"})]
        [TestCase("cc", new [] {"cc"})]
        [TestCase("abb", new [] {"abb", "bab", "bba"})]
        [TestCase("xxy", new [] {"xxy", "xyx", "yxx"})]
        [TestCase("ded", new [] {"ded", "dde", "edd"})]
        [TestCase("abc", new [] {"abc", "acb", "bca", "bac", "cab", "cba"})]
        [TestCase("xyz", new [] {"xyz", "xzy", "yzx", "yxz", "zxy", "zyx"})]
        [TestCase("xxx", new [] {"xxx"})]
        [TestCase("iiiiiiiiiiiiiiiiiiii", new [] {"iiiiiiiiiiiiiiiiiiii"})]
        [TestCase("vvvvvvvvvvvv", new [] {"vvvvvvvvvvvv"})]
        public void get_all_anagrams(string input, string[] expectedResult)
        {
            string[] result = Anagram.Parse(input);
            
            Assert.AreEqual(new string[]{}, expectedResult.Except(result));
        }
    }
}