﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Anagram
{
    public class Anagram
    {
        public static string[] Parse(string input)
        {
            var result = new List<string>();
            
            int indexSwap = 0;
            string currentAnagram = input;
            while (!result.Contains(currentAnagram))
            {
                result.Add(currentAnagram);

                while (IsNextPositionEqual(currentAnagram, indexSwap))
                {
                    indexSwap = IncreaseIndexSwap(currentAnagram, indexSwap);
                    if (indexSwap == 0)
                        break;
                }
                
                currentAnagram = SwapWithNextPosition(currentAnagram, indexSwap);
                
                indexSwap = IncreaseIndexSwap(currentAnagram, indexSwap);
            }

            return result.ToArray();
        }
        
        private static bool IsNextPositionEqual(string anagram, int index)
        {
            if (index + 1 == anagram.Length)
                return anagram[0] == anagram[index];
            
            return anagram[index] == anagram[index + 1];

        }

        private static int IncreaseIndexSwap(string input, int indexSwap)
        {
            if (indexSwap == input.Length - 1)
                indexSwap = 0;
            else
                indexSwap++;
            return indexSwap;
        }

        private static string SwapWithNextPosition(string input, int position)
        {
            var chars = input.ToCharArray();
            
            var temp = chars[position];
            
            if (IsLastPosition(input, position))
            {
                chars[position] = chars[0];
                chars[0] = temp;
            }
            else
            {
                chars[position] = chars[position + 1];
                chars[position + 1] = temp;
            }
            
            return new string(chars);
        }
        
        private static bool IsLastPosition(string input, int position)
        {
            return position == input.Length - 1;
        }
    }
}